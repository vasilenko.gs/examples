<?php

class Book { 
    const A = 12421;
    public $a;
    private $b;

    function __construct() {
        $this->a = 'public $a';
        $this->b = 'private $b';
    }

    public function getPublicVar(){
        return $this->a;
    }
    public function getPublicVarViaPrivate(){
        return $this->a;
    }
    public function getPublicVarViaPublicUsingPrivate(){
        return $this->a;
    }
    public function getPrivateVar(){
        return $this->b;
    }
    public function getPrivateVarViaPrivate(){
        return $this->b;
    }
    public function getPrivateVarViaPublicUsingPrivate(){
        return $this->b;
    }
}
class ExtendedBook extends Book { 
    public static $public_counter;
    private static $private_counter;
    public function getPublicCounterViaPublic(){
        return self::$public_counter;
    }
    private function getPublicCounterViaPrivate(){
        return self::$public_counter;
    }
    public function getPublicCounterViaPublicUsingPrivate(){
        return self::getPublicCounterViaPrivate();
    }
    public function getPrivateCounterViaPublic(){
        return self::$private_counter;
    }
    private function getPrivateCounterViaPrivate(){
        return self::$private_counter;
    }
    public function getPrivateCounterViaPublicUsingPrivate(){
        return self::getPrivateCounterViaPrivate();
    }
    function __construct() {
        self::$public_counter++;
        self::$private_counter++;
        self::$private_counter++;
        parent::__construct();
    }
}

$instance_of_book = new Book();
echo "константа A класса Book вызванная через класс: ";
echo Book::A . '<br>';
echo "константа A класса Book вызванная через экземпляр класса: ";
echo $instance_of_book::A . '<br><br>';
echo 'public поле класса Book вызванное напрямую: ';
echo $instance_of_book->a . '<br><br>';
echo 'public поле класса Book вызванное через public функцию: ';
echo $instance_of_book->getPublic() . '<br><br>';
// echo 'var $b: ';
// echo $instance_of_book->b . '<br><br>';
echo 'var $b: ';
echo $instance_of_book->getPrivate() . '<br><br>';

$books = [];
$extended_books = [];
for($i=0;$i<10;$i++){
    $books[] = new Book();
    $extended_books = new ExtendedBook();
}
// echo $books[0]::A;

echo '$public_counter вызыванный напрямую: ';
echo ExtendedBook::$public_counter . '<br><br>';
echo '$public_counter вызванный через public метод класса: ';
echo ExtendedBook::getPublicCounterViaPublic() . '<br><br>';
echo '$public_counter вызванный через private метод класса: ';
try {
    echo ExtendedBook::getPublicCounterViaPrivate() . '<br><br>';
} catch (\Throwable $th) {
    echo $th->getMessage(). '<br><br>';
}
echo '$public_counter вызванный через public метод класса, использующий private метод класса: ';
echo ExtendedBook::getPublicCounterViaPublicUsingPrivate() . '<br><br>';

echo '$private_counter вызыванный напрямую: ';
try {
    echo ExtendedBook::$private_counter . '<br><br>';
} catch (\Throwable $th) {
    echo $th->getMessage(). '<br><br>';
}
echo '$private_counter вызванный через public метод класса: ';
echo ExtendedBook::getPrivateCounterViaPublic() . '<br><br>';
echo '$private_counter вызванный через private метод класса: ';
try {
    echo ExtendedBook::getPrivateCounterViaPrivate() . '<br><br>';
} catch (\Throwable $th) {
    echo $th->getMessage(). '<br><br>';
}
echo '$public_counter вызванный через private метод класса, использующий private метод класса: ';
echo ExtendedBook::getPrivateCounterViaPublicUsingPrivate() . '<br><br>';

?>